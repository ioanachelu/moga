(function(){
    function init_map() {

      	var location = new google.maps.LatLng(44.417184,26.119362);
 		
        var mapoptions = {
        	zoom: 14,
		    center: location,
		    scrollwheel: false,
		    disableDefaultUI: true,
		    draggable: false
		   };

        var pinSize = 730/2;
 		    var currentPinImage = {
      		url: 'images/MAPPOINTER.svg',
      		scaledSize: new google.maps.Size(pinSize, pinSize),
      		anchor: new google.maps.Point(pinSize/2+1, pinSize/2+30),
    	   };

    	   var map = new google.maps.Map(document.getElementById("map-container-g"),
            mapoptions);
    	   var currentPin = new google.maps.Marker({
      		   position: location,
      		   map: map,
      		   icon: currentPinImage
    	 });

    	  // Keep the map responsive
        var center;
    		function calculateCenter() {
      				center = map.getCenter();
    		}
    		google.maps.event.addDomListener(map, 'idle', function() {
    		  calculateCenter();
    		});
    		google.maps.event.addDomListener(window, 'resize', function() {
    		  map.setCenter(center);
    		});
          }

    //google.maps.event.addDomListener(window, 'load', init_map);
    
    $(function () {
        $(window).on("load", init_map);
    });
}());