(function(){
    
    var mainModule = angular.module('MainModule');

    mainModule.controller('MainCtrl', function ($scope) {
      $scope.myInterval = 5000;
      var slides = $scope.slides = [];
      $scope.addSlide = function(i, total) {
        var newWidth = 600 + slides.length + 1;
        slides.push({
          image: './images/nanoGallery/intro_img' + (i + 1) + '.jpg',
          text: ['Moga Group','Ruby','Moga'][slides.length % total] 
        });
      };
      var total = 3;
      for (var i=0; i<total; i++) {
        $scope.addSlide(i, total);
      }
    });
    
}());