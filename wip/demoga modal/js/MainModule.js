(function () {
    
    var mainModule = angular.module('MainModule', ['ui.bootstrap', 'ngRoute']);
    
    mainModule.config(function($routeProvider){
        $routeProvider
            .when("/main", {
                templateUrl: "main.html",
                controller: "MainCtrl",
            })
            .when("/studios", {
                templateUrl: "studios.html",
                controller: "StudiosCtrl",
            })
            .otherwise({redirectTo: "/main"});
    });
    
    mainModule.directive('helloMaps', function () {
      return function (scope, elem, attrs) {
             var mapOptions,
                latitude = attrs.latitude,
                longitude = attrs.longitude,
                map;

             latitude = latitude && parseFloat(latitude, 10) || 44.417184;
             longitude = longitude && parseFloat(longitude, 10) || 26.119362;
             var location = new google.maps.LatLng(latitude, longitude);
             var pinSize = 730 / 2;
             var currentPinImage = {
                 url: 'images/MAPPOINTER.svg',
                 scaledSize: new google.maps.Size(pinSize, pinSize),
                 anchor: new google.maps.Point(pinSize / 2 + 1, pinSize / 2 + 30),
             };

             mapOptions = {
                 center: location,
                 zoom: 14,
                 scrollwheel: false,
                 disableDefaultUI: true,
                 draggable: false
             };
             map = new google.maps.Map(elem[0], mapOptions);
             var currentPin = new google.maps.Marker({
                 position: location,
                 map: map,
                 icon: currentPinImage
             });

             // Keep the map responsive
             var center;

             function calculateCenter() {
                 center = map.getCenter();
             }
             google.maps.event.addDomListener(map, 'idle', function() {
                 calculateCenter();
             });
             google.maps.event.addDomListener(window, 'resize', function() {
                 map.setCenter(center);
             });
        }
    });
    
}());
 
